# UE LIFPF INF2030L Programmation Fonctionnelle

## Semestre 2023 Printemps

Attention: **lundi 16/01/2023 groupes B et E** le TD est déplacé à 11h30 <del>dans le bâtiment Nautibus, salle C1 (pour les 2 groupes)</del>, salle à confirmer

| jour  | heure | type | supports / remarques                                         |
| ----- | ----- | ---- | ------------------------------------------------------------ |
| 16/01 | 8h    | CM   | [Diapositives](cm/lifpf-cm1.pdf)                             |
|       | 9h45  | TD   | [Sujet](td/lifpf-td1-enonce.pdf) <br> Groupes B et E à 11h30 |
